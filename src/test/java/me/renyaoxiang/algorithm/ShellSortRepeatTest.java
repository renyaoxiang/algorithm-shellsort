package me.renyaoxiang.algorithm;

import io.qameta.allure.*;
import me.renyaoxiang.algorithm.sort.ArrayUtils;
import me.renyaoxiang.algorithm.sort.ImprovedShellSort;
import me.renyaoxiang.algorithm.sort.ShellSort;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DisplayName("ShellSort test repeatedly")
class ShellSortRepeatTest {
    static int count = 10;

    @Order(11)
    @DisplayName("ShellSortForStats")
    @ParameterizedTest(name = "{displayName}")
    @ArgumentsSource(SortArguments1000Provider.class)
    @Description("ShellSortForStats Description.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("renyaoxiang")
    @Tag("ShellSortForStats Tag")
    @Epic("ShellSortForStats Epic")
    @Feature("ShellSortForStats Feature")
    @Story("ShellSortForStats Story")
    @AllureId("101")
    void testGenericShellSort(int[] expectData, int[] inputData) {
        ShellSort.sort(inputData);
        Assertions.assertEquals(Arrays.toString(expectData), Arrays.toString(inputData));
    }

    @ParameterizedTest()
    @ArgumentsSource(SortArguments1000Provider.class)
    @DisplayName("优化版希尔排序")
    @Description("ImprovedShellSort Description.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("renyaoxiang")
    @Tag("ImprovedShellSort Tag")
    @Epic("ImprovedShellSort Epic")
    @Feature("ImprovedShellSort Feature")
    @Story("ImprovedShellSort Story")
    @AllureId("102")
    void testImprovedShellSort(int[] expectData, int[] inputData) {
        ImprovedShellSort.sort(inputData);
        Assertions.assertEquals(Arrays.toString(expectData), Arrays.toString(inputData));
    }

    static class SortArguments1000Provider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
            return IntStream.range(0, count).mapToObj(it -> {
                return createArguments(context);
            });
        }

        Arguments createArguments(ExtensionContext context) {
            int[] expectData = IntStream.range(0, 1000).toArray();
            int[] inputData = IntStream.range(0, 1000).toArray();
            ArrayUtils.shuffle(inputData, 0, inputData.length);
            Arguments arguments = Arguments.arguments(expectData, inputData);
            return arguments;
        }
    }
}