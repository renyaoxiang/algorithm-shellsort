package me.renyaoxiang.algorithm.sortforstats;

import me.renyaoxiang.algorithm.sort.ArrayUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ShellSortForStatsTest {
    public static int[] inputData0, inputData1, inputData2, inputData3, inputData4, inputData5;

    @BeforeAll
    static void doBeforeAll() {
        inputData0 = ArrayUtils.shuffle(ArrayUtils.range(0, 1000));
        inputData1 = ArrayUtils.cloneArray(inputData0);
        inputData2 = ArrayUtils.cloneArray(inputData0);
        inputData3 = ArrayUtils.cloneArray(inputData0);
        inputData4 = ArrayUtils.cloneArray(inputData0);
        inputData5 = ArrayUtils.cloneArray(inputData0);
    }

    @Test
    void sortGenericShellSortForStats() {
        StatsContext.start(it -> {
            ShellSortForStats.sort(it, inputData1);
            it.show("ShellSortForStats");
        });
    }

    @Test
    void sortImprovedShellSortForStats() {
        StatsContext.start(it -> {
            ImprovedShellSortForStats.sort(it, inputData2);
            it.show("ImprovedShellSortForStats");
        });
    }

    @Test
    void sortSimpleInsertSortForStats() {
        StatsContext.start(it -> {
            InsertSortForStats.sort(it, inputData3);
            it.show("InsertSortForStats");
        });
    }

    @Test
    void sortSimpleBubbleSortForStats() {
        StatsContext.start(it -> {
            BubbleSortForStats.sort(it, inputData4);
            it.show("BubbleSortForStats");
        });
    }

    @Test
    void sortSimpleQuickSortForStats() {
        StatsContext.start(it -> {
            QuickSortForStats.sort(it, inputData5);
            it.show("QuickSortForStats");
        });
    }
}