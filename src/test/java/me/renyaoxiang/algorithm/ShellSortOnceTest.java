package me.renyaoxiang.algorithm;

import io.qameta.allure.*;
import me.renyaoxiang.algorithm.sort.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DisplayName("ShellSort test once")
class ShellSortOnceTest {

    @Order(11)
    @DisplayName("ShellSortForStats")
    @ParameterizedTest
    @ArgumentsSource(SortArgumentsProvider.class)
    @Description("ShellSortForStats Description.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("renyaoxiang")
    @Tag("ShellSortForStats Tag")
    @Epic("ShellSortForStats Epic")
    @Feature("ShellSortForStats Feature")
    @Story("ShellSortForStats Story")
    @AllureId("1")
    void testGenericShellSort(int[] expectData, int[] inputData) {
        ShellSort.sort(inputData);
        Assertions.assertEquals(Arrays.toString(expectData), Arrays.toString(inputData));
    }

    @ParameterizedTest()
    @ArgumentsSource(SortArgumentsProvider.class)
    @DisplayName("ImprovedShellSort")
    @Description("ImprovedShellSort Description.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("renyaoxiang")
    @Tag("ImprovedShellSort Tag")
    @Epic("ImprovedShellSort Epic")
    @Feature("ImprovedShellSort Feature")
    @Story("ImprovedShellSort Story")
    @AllureId("2")
    void testImprovedShellSort(int[] expectData, int[] inputData) {
        ImprovedShellSort.sort(inputData);
        Assertions.assertEquals(Arrays.toString(expectData), Arrays.toString(inputData));
    }

    @ParameterizedTest()
    @ArgumentsSource(SortArgumentsProvider.class)
    @DisplayName("InsertSort")
    @Description("InsertSort Description.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("renyaoxiang")
    @Tag("InsertSort Tag")
    @Epic("InsertSort Epic")
    @Feature("InsertSort Feature")
    @Story("InsertSort Story")
    @AllureId("2")
    void testSimpleInsertSort(int[] expectData, int[] inputData) {
        InsertSort.sort(inputData);
        Assertions.assertEquals(Arrays.toString(expectData), Arrays.toString(inputData));
    }

    @ParameterizedTest()
    @ArgumentsSource(SortArgumentsProvider.class)
    @DisplayName("BubbleSort")
    @Description("BubbleSort Description.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("renyaoxiang")
    @Tag("BubbleSort Tag")
    @Epic("BubbleSort Epic")
    @Feature("BubbleSort Feature")
    @Story("BubbleSort Story")
    @AllureId("2")
    void testSimpleBubbleSort(int[] expectData, int[] inputData) {
        BubbleSort.sort(inputData);
        Assertions.assertEquals(Arrays.toString(expectData), Arrays.toString(inputData));
    }

    @ParameterizedTest()
    @ArgumentsSource(SortArgumentsProvider.class)
    @DisplayName("QuickSort")
    @Description("QuickSort Description.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("renyaoxiang")
    @Tag("QuickSort Tag")
    @Epic("QuickSort Epic")
    @Feature("QuickSort Feature")
    @Story("QuickSort Story")
    @AllureId("2")
    void testSimpleQuickSort(int[] expectData, int[] inputData) {
        QuickSort.sort(inputData);
        Assertions.assertEquals(Arrays.toString(expectData), Arrays.toString(inputData));
    }
    static class SortArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
            return Stream.of(createArguments(context));
        }

        Arguments createArguments(ExtensionContext context) {
            int[] expectData = IntStream.range(0, 1000).toArray();
            int[] inputData = IntStream.range(0, 1000).toArray();
            ArrayUtils.shuffle(inputData, 0, inputData.length);
            Arguments arguments = Arguments.arguments(expectData, inputData);
            return arguments;
        }
    }
}