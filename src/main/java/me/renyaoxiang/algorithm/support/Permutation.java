package me.renyaoxiang.algorithm.support;

import java.util.Arrays;
import java.util.function.Consumer;

public class Permutation {
    int[] dataArray;

    public Permutation(int[] dataArray) {
        this.dataArray = dataArray;
    }

    public void visit(Consumer<int[]> onVisit) {
        visit(dataArray.length, onVisit);
    }

    public void visit(int length, Consumer<int[]> onVisit) {
        int[] visitedState = new int[dataArray.length];
        Arrays.fill(visitedState, 1);
        int[] newDataArray = Arrays.copyOf(dataArray, dataArray.length);
        doVisit(newDataArray, length, onVisit, new int[dataArray.length], 0, visitedState);
    }

    private void doVisit(int[] dataArray, int length, Consumer<int[]> onVisit, int[] context, int validContextIndex, int[] visitedState) {
        if (validContextIndex == length) {
            onVisit.accept(Arrays.copyOf(context, length));
        } else {
            for (int i = 0; i < dataArray.length; i++) {
                if (visitedState[i] > 0) {
                    visitedState[i]--;
                    context[validContextIndex] = dataArray[i];
                    doVisit(dataArray, length, onVisit, context, validContextIndex + 1, visitedState);
                    visitedState[i]++;
                }
            }
        }
    }
}
