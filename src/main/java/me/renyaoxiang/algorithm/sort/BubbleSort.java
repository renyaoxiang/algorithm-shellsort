package me.renyaoxiang.algorithm.sort;

public class BubbleSort {
    public static void sort(int[] data) {
        for (int i = 0; i < data.length; i++) {
            for (int j = data.length - 1; j > 0; j--) {
                if (data[j] < data[j - 1]) {
                    ArrayUtils.swap(data, j, j - 1);
                }
            }
        }
    }
}
