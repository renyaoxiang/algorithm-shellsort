package me.renyaoxiang.algorithm.sort;

import java.security.SecureRandom;
import java.util.Random;
import java.util.stream.IntStream;

public final class ArrayUtils {

    public static void swap(int[] array, int first, int second) {
        int temp = array[first];
        array[first] = array[second];
        array[second] = temp;
    }

    public static int[] range(int start, int end) {
        return IntStream.range(start, end).toArray();
    }

    public static int[] shuffle(int[] data) {
        shuffle(data, 0, data.length);
        return data;
    }

    public static void shuffle(int[] data, int startInclude, int endExclude) {
        Random random = new SecureRandom();
        for (int i = startInclude; i < endExclude; i++) {
            swap(data, i, random.nextInt(startInclude, endExclude));
        }
    }

    public static int[] cloneArray(int[] data) {
        int[] array = new int[data.length];
        for (int i = 0; i < data.length; i++) {
            array[i] = data[i];
        }
        return array;
    }
}
