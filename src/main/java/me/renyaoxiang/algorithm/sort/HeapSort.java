package me.renyaoxiang.algorithm.sort;

import java.util.ArrayList;
import java.util.List;

public class HeapSort {
    List<Integer> dataList = new ArrayList<>();

    public static void sort(int[] data) {
        HeapSort heapSort = new HeapSort();
        for (int i = 0; i < data.length; i++) {
            heapSort.push(data[i]);
        }
        for (int i = 0; i < data.length; i++) {
            data[i] = heapSort.pop();
        }
    }

    private int pop() {
        swap(dataList, 0, dataList.size() - 1);
        int minData = dataList.remove(dataList.size() - 1);
        int current = 0;
        while (2 * current + 1 < dataList.size()) {
            int left = 2 * current + 1;
            int right = left+1;
            int minIndex = left;
            if (hasData(right) && dataList.get(left) > dataList.get(right)) {
                minIndex = right;
            }
            if (dataList.get(current) > dataList.get(minIndex)) {
                swap(dataList, minIndex, current);
                current = minIndex;
            } else {
                break;
            }
        }
        return minData;
    }

    private boolean hasData(int index) {
        return index < dataList.size();
    }

    private void push(int it) {
        dataList.add(it);
        int current = dataList.size() - 1;
        int parent = (current - 1) / 2;
        while (current != parent && dataList.get(current) < dataList.get(parent)) {
            swap(dataList, current, parent);
            current = parent;
            parent = (current - 1) / 2;
        }
    }

    private static void swap(List<Integer> dataList, int first, int second) {
        int temp = dataList.get(first);
        dataList.set(first, dataList.get(second));
        dataList.set(second, temp);
    }
}
