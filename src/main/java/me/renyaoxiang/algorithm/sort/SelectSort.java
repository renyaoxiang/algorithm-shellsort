package me.renyaoxiang.algorithm.sort;

public class SelectSort {
    public static void sort(int[] data) {
        for (int i = 0; i < data.length; i++) {
            int maxIndex = 0;
            for (int j = 0; j < data.length - i; j++) {
                if (data[j] > data[maxIndex]) {
                    maxIndex = j;
                }
            }
            ArrayUtils.swap(data, data.length - i - 1, maxIndex);
        }
    }
}
