package me.renyaoxiang.algorithm.sort;

public class ShellSort {
    public static void sort(int[] data) {
        for (int step = data.length; step > 0; step = step / 2) {
            for (int head = step; head < data.length; head++) {
                for (int toInsert = head; toInsert >= 0; toInsert -= step) {
                    int pre = toInsert - step;
                    if (pre >= 0 && data[toInsert] < data[pre]) {
                        ArrayUtils.swap(data, toInsert, pre);
                    }
                }
            }
        }
    }
}
