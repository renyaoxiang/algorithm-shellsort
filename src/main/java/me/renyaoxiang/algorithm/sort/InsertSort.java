package me.renyaoxiang.algorithm.sort;

public class InsertSort {
    public static void sort(int[] data) {
        for (int i = 0; i < data.length; i++) {
            for (int j = i; j > 0; j--) {
                if (data[j] < data[j - 1]) {
                    ArrayUtils.swap(data, j, j - 1);
                } else {
                    break;
                }
            }
        }
    }
}
