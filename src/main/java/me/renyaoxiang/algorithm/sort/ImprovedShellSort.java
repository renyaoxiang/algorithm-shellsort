package me.renyaoxiang.algorithm.sort;

public class ImprovedShellSort {
    public static void sort(int[] data) {
        for (int step = data.length; step > 0; step = step / 2) {
            for (int group = 0; group < step; group++) {
                for (int head = group; head < data.length; head += step) {
                    for (int toInsert = head; toInsert >= 0; toInsert -= step) {
                        int pre = toInsert - step;
                        if (pre >= 0 && data[toInsert] < data[pre]) {
                            ArrayUtils.swap(data, toInsert, pre);
                        } else {
                            break;
                        }
                    }
                }
            }
        }
    }
}
