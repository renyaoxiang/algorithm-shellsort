package me.renyaoxiang.algorithm.sortforstats;

public interface Steps {
    String INDEX_SIGN = "index_SIGN";
    String INDEX_COMPARE = "index_compare";
    String COMPARE = "compare";
    String SIGN = "sign";
}
