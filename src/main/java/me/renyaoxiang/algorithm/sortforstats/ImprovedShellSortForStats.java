package me.renyaoxiang.algorithm.sortforstats;

import me.renyaoxiang.algorithm.sort.ArrayUtils;

public class ImprovedShellSortForStats {
    public static void sort(StatsContext statsContext, int[] data) {

        statsContext.callIndexSign();
        for (int step = data.length; step > 0; step = step / 2) {
            statsContext.callIndexCompare();
            statsContext.callIndexSign();

            statsContext.callIndexSign();
            for (int group = 0; group < step; group++) {
                statsContext.callIndexCompare();
                statsContext.callIndexSign();

                statsContext.callIndexSign();
                for (int head = group; head < data.length; head += step) {
                    statsContext.callIndexCompare();
                    statsContext.callIndexSign();

                    statsContext.callIndexSign();
                    for (int toInsert = head; toInsert >= 0; toInsert -= step) {
                        statsContext.callIndexCompare();
                        statsContext.callIndexSign();

                        statsContext.callIndexSign();
                        int pre = toInsert - step;

                        statsContext.callCompare();
                        statsContext.callCompare();
                        if (pre >= 0 && data[toInsert] < data[pre]) {

                            statsContext.callSwap();
                            ArrayUtils.swap(data, toInsert, pre);
                        } else {
                            break;
                        }
                    }
                }
            }
        }
    }
}
