package me.renyaoxiang.algorithm.sortforstats;

import me.renyaoxiang.algorithm.sort.ArrayUtils;

public class InsertSortForStats {
    public static void sort(StatsContext statsContext, int[] data) {
        statsContext.callIndexSign();
        for (int i = 0; i < data.length; i++) {
            statsContext.callIndexCompare();
            statsContext.callIndexSign();


            statsContext.callIndexSign();
            for (int j = i; j > 0; j--) {
                statsContext.callIndexCompare();
                statsContext.callIndexSign();

                statsContext.callCompare();
                if (data[j] < data[j - 1]) {
                    statsContext.callSwap();
                    ArrayUtils.swap(data, j, j - 1);
                } else {
                    break;
                }
            }
        }
    }
}
