package me.renyaoxiang.algorithm.sortforstats;

import me.renyaoxiang.algorithm.sort.ArrayUtils;

public class ShellSortForStats {
    public static void sort(StatsContext statsContext, int[] data) {

        statsContext.callIndexSign();
        for (int step = data.length; step > 0; step = step / 2) {
            statsContext.callIndexCompare();
            statsContext.callIndexSign();


            statsContext.callIndexSign();
            for (int head = step; head < data.length; head++) {
                statsContext.callIndexCompare();
                statsContext.callIndexSign();


                statsContext.callIndexSign();
                for (int toInsert = head; toInsert >= 0; toInsert -= step) {
                    statsContext.callIndexCompare();
                    statsContext.callIndexSign();

                    statsContext.callIndexSign();
                    int pre = toInsert - step;

                    statsContext.callIndexCompare();
                    if (pre >= 0 ) {
                        statsContext.callCompare();
                        if( data[toInsert] < data[pre]){
                            statsContext.callSwap();
                            ArrayUtils.swap(data, toInsert, pre);
                        }
                    }
                }
            }
        }
    }
}
