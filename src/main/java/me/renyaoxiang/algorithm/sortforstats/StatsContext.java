package me.renyaoxiang.algorithm.sortforstats;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class StatsContext {
    Map<String, BigInteger> callCountMap = new HashMap<>();

    public static StatsContext start(Consumer<StatsContext> contextConsumer) {
        StatsContext statsContext = new StatsContext();
        contextConsumer.accept(statsContext);
        return statsContext;
    }

    public void callSwap() {
        call(Steps.SIGN, 2);
    }

    public void callIndexSign() {
        call(Steps.INDEX_SIGN);
    }
    public void callSign() {
        call(Steps.SIGN);
    }

    public void callCompare() {
        call(Steps.COMPARE);
    }

    public void callIndexCompare() {
        call(Steps.INDEX_COMPARE);
    }

    private void call(String action, int count) {
        BigInteger current = callCountMap.getOrDefault(action, BigInteger.ZERO);
        callCountMap.put(action, current.add(BigInteger.valueOf(count)));
    }

    private void call(String action) {
        BigInteger count = callCountMap.getOrDefault(action, BigInteger.ZERO);
        callCountMap.put(action, count.add(BigInteger.ONE));
    }

    public void show(String title) {
        callCountMap.forEach((key, value) -> {
            System.out.println(key + ":\t\t" + value);
        });
    }

    public int getIntValue(String compare) {
        return callCountMap.getOrDefault(compare, BigInteger.ZERO).intValue();
    }
}
