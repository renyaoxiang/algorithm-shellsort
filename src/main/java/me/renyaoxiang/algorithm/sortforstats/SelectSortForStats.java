package me.renyaoxiang.algorithm.sortforstats;

import me.renyaoxiang.algorithm.sort.ArrayUtils;

public class SelectSortForStats {
    public static void sort(StatsContext statsContext, int[] data) {
        statsContext.callIndexSign();
        for (int sortedLength = 0; sortedLength < data.length; sortedLength++) {
            statsContext.callIndexSign();
            statsContext.callIndexCompare();

            statsContext.callIndexSign();
            int maxIndex = 0;

            statsContext.callIndexSign();
            for (int unsortedToCheckIndex = 0; unsortedToCheckIndex < data.length - sortedLength; unsortedToCheckIndex++) {
                statsContext.callIndexSign();
                statsContext.callIndexCompare();

                statsContext.callCompare();
                if (data[unsortedToCheckIndex] > data[maxIndex]) {

                    statsContext.callIndexSign();
                    maxIndex = unsortedToCheckIndex;
                }
            }

            statsContext.callSwap();
            ArrayUtils.swap(data, data.length - sortedLength - 1, maxIndex);
        }
    }
}
