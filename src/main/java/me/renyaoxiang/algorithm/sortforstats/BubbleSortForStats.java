package me.renyaoxiang.algorithm.sortforstats;


import me.renyaoxiang.algorithm.sort.ArrayUtils;

public class BubbleSortForStats {
    public static void sort(StatsContext statsContext, int[] data) {

        statsContext.callIndexSign();
        for (int sortedLength = 0; sortedLength < data.length; sortedLength++) {
            statsContext.callIndexCompare();
            statsContext.callIndexSign();

            statsContext.callIndexSign();
            for (int unsortedToCheckIndex = data.length - 1; unsortedToCheckIndex > 0; unsortedToCheckIndex--) {
                statsContext.callIndexCompare();
                statsContext.callIndexSign();

                statsContext.callIndexSign();
                int preUnsortedToCheckIndex = unsortedToCheckIndex - 1;

                statsContext.callCompare();
                if (data[unsortedToCheckIndex] < data[preUnsortedToCheckIndex]) {

                    statsContext.callSwap();
                    ArrayUtils.swap(data, unsortedToCheckIndex, preUnsortedToCheckIndex);
                }
            }
        }
    }
}
